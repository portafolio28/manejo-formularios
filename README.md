# manejo-formularios

Este es un Microservicio desarrollado en Quarkus enfocado en generar formularios PDF para almacenarlos en un FTP y volúmenes de Docker a partir de un JSON. Utilizado para el aplicativo de pensiones de Bolivia.

Por motivos de privacidad este contenido está estrechamente limitado y se le deshabilito la ejecución correcta en local para evitar plagios o suplantación.
